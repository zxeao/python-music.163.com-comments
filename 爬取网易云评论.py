'''
Author: Zxeao
Date: 2021-08-09 07:56:59
LastEditors: Zxeao
LastEditTime: 2021-08-10 09:22:13
Description: file content
'''
from Crypto.Cipher import AES
from base64 import b64encode
import requests
import json
import string
import re

#1.找到未加密的参数
#2.想办法把参数进行加密（用网易的加密方式）
#3.请求到网易，拿到评论信息

url = "https://music.163.com/weapi/comment/resource/comments/get?csrf_token="

# 请求方式是 post
data = {
    "csrf_token": "",
    "cursor": "-1",
    "offset": "0",
    "orderType": "1",
    "pageNo": "1",
    "pageSize": "20",
    "rid": "R_SO_4_516076896",   # R_SO_4_1330348068
    "threadId": "R_SO_4_516076896",
}

# print(data)
# print(type(data))
# print(type(json.dumps(data)))

'''
2
csrf_token: ""
cursor: "1628482874083"
offset: "40"
orderType: "1"
pageNo: "2"
pageSize: "20"
rid: "R_SO_4_1330348068"
threadId: "R_SO_4_1330348068"
3
csrf_token: ""
cursor: "1628482991257"
offset: "60"
orderType: "1"
pageNo: "3"
pageSize: "20"
rid: "R_SO_4_1330348068"
threadId: "R_SO_4_1330348068"



6
csrf_token: ""
cursor: "1628523314505"
offset: "40"
orderType: "1"
pageNo: "6"
pageSize: "20"
rid: "R_SO_4_1330348068"
threadId: "R_SO_4_1330348068"


9
csrf_token: ""
cursor: "1628522023029"
offset: "40"
orderType: "1"
pageNo: "9"
pageSize: "20"
rid: "R_SO_4_1330348068"
threadId: "R_SO_4_1330348068"



10
csrf_token: ""
cursor: "1628521605280"
offset: "40"
orderType: "1"
pageNo: "10"
pageSize: "20"
rid: "R_SO_4_1330348068"
threadId: "R_SO_4_1330348068"


'''

headers = {
    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36 Edg/92.0.902.67"
}

proxies = {
    "http":"http://88.99.10.254:1080"
}

obj = re.compile(r'.*?,"nickname":"(?P<nickname>.*?)",".*?,"content":"(?P<content>.*?)","',re.S)

# 处理加密过程

e = "010001"
f = "00e0b509f6259df8642dbc35662901477df22677ec152b5ff68ace615bb7b725152b3ab17a876aea8a5aa76d2e417629ec4ee341f56135fccf695280104e0312ecbda92557c93870114af6c9d05c4f7f0c3685b7a46bee255932575cce10b424d813cfe4875d3e82047b97ddef52741d546b8e289dc6935b3ece0462db0a22b8e7""00e0b509f6259df8642dbc35662901477df22677ec152b5ff68ace615bb7b725152b3ab17a876aea8a5aa76d2e417629ec4ee341f56135fccf695280104e0312ecbda92557c93870114af6c9d05c4f7f0c3685b7a46bee255932575cce10b424d813cfe4875d3e82047b97ddef52741d546b8e289dc6935b3ece0462db0a22b8e7"
g = "0CoJUm6Qyw8W8jud"
i = "Pw0F6aFMG6lnuXoJ"

def get_encSecKey():
    return "d8b365048a3402ec663027de5699cc92ea073f0b8089c9325b748b9c325f4cbe986087d8f98a4d32c5c049a9519e2e0e2aad86f83acc5c08af0bd836573f2502c8a8bb4c2787f3b52279d648fa353992de5982e802a7ae24e249f077e1e05f498acff3908f4752c219a5a5950351add287bf70f84d6f01367cc0f63d651595ab"

def get_params(data):
    first = enc_param(data,g)
    second = enc_param(first,i)
    return second

def to_16(data):
    pad = 16-len(data)%16
    data +=chr(pad) * pad
    return data


def enc_param(data,key):
    iv = "0102030405060708"
    data = to_16(data)
    # print(type(key))
    aes = AES.new(key=key.encode('utf-8'),IV=iv.encode('utf-8'), mode=AES.MODE_CBC)
    bs = aes.encrypt(data.encode('utf-8'))  #加密   加密的内容必须是16的倍数
    return str(b64encode(bs),"utf-8")

f = open("《纸短情长》.txt",mode="w",encoding="utf-8")

resp = requests.post(url,data={
    "params":get_params(json.dumps(data)),
    "encSecKey":get_encSecKey()
},headers=headers,proxies=proxies)

data['cursor'] = re.search('.*?,"cursor":"(?P<cursor>.*?)","', resp.text).group("cursor")
data['offset'] ="40"
data['pageNo'] = str(int(data['pageNo'])+1)

list = obj.findall(resp.text)

resp.close()

for lis in list:
    f.writelines(str(lis).replace('\\\\n','')+"\n")

# print(data)
# print(type(data))
# print(type(json.dumps(data)))


for j in range(2,32511):
    # print(data)
    print(j)
    resp = requests.post(url,data={
    "params":get_params(json.dumps(data)),
    "encSecKey":get_encSecKey()
    },headers=headers,proxies=proxies)
    print("!!!")

    data['cursor'] = re.search('.*?,"cursor":"(?P<cursor>.*?)","', resp.text).group("cursor")
    print(data['cursor'])
    # data['offset'] = str(int(data['offset'])+20)
    data['pageNo'] = str(int(data['pageNo'])+1)

    list = obj.findall(resp.text)

    resp.close()

    for k in list:
        f.writelines(str(k).replace('\\\\n','')+"\n")

'''
    encText: "UwiQDALA6D0iBEG6N/qyO3X8ybCrnKHXE/4EHwfq7Q0rD7ADC3j75BOOsPYQhHHmLiBXHazIW+3Og7uX8IbdZX7vg8uvMNRpgxCXDtXq1NxWlRw+VDCvLbkD7V0O/kyI"

    function a(a) {
        var d, e, b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", c = "";
        for (d = 0; a > d; d += 1)
            e = Math.random() * b.length,
            e = Math.floor(e),
            c += b.charAt(e);
        return c
    }
    function b(a, b) {
        var c = CryptoJS.enc.Utf8.parse(b)
          , d = CryptoJS.enc.Utf8.parse("0102030405060708")
          , e = CryptoJS.enc.Utf8.parse(a)
          , f = CryptoJS.AES.encrypt(e, c, {
            iv: d,
            mode: CryptoJS.mode.CBC
        });
        return f.toString()
    }
    function c(a, b, c) {
        var d, e;
        return setMaxDigits(131),
        d = new RSAKeyPair(b,"",c),
        e = encryptedString(d, a)
    }
    function d(d, e, f, g) {  d=data
        var h = {}
          , i = a(16);
        return h.encText = b(d, g),
        h.encText = b(h.encText, i),
        h.encSecKey = c(i, e, f),
        h
    }
'''
f.close()
